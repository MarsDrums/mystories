########################################
###                                  ###
###        Arch Linux Install        ###
###           December 2021          ###
###                                  ###
########################################
#    This file is best used with the    #
#     December 2021 & January 2022     #
#        releases of Arch Linux        # 
#                                      #
#    https://archlinux.org/download/   #
#                 For                  #
# https://gitlab.com/Phydoux/archlinux #
########################################
#                 NOTE                 #
# It's better to download this file and #
#  read it in a notepad or file editor  #
#    with word wrapping abilities.     #
########################################

## Adjust Screen Resolution
e or E at menu screen. (Optional)
Type video=1920x1080

## Make Font Larger (Optional)
setfont ter-132n

## List Keymaps (Not Necessary if using US keyboard)
localectl list-keymaps

## View Internet Interfaces
ip -c a

##Update the system clock
timedatectl set-ntp true

## Check Drives
lsblk

## Partitioning HDD Using cfdisk
cfdisk /dev/(This will be whatever drive you wish to use from the lsblk list. sda, sdb, sdc... )
Select dos for regular Boot
Select Drive to Partition Then New
Partition Size: 300M - primary -Note the M for Megabytes. NOT Gigabytes!

Arrow down to Free Space
Partition Size: 25GB - primary

Arrow down to Free Space
Partion Size: Remaining Disk Space - primary

All  ID types should be 83 Linux

Select Write then type yes
Select Quit

lsblk to see disk partitions You should see 3 partitions added now

## Format Partitions
mkfs.ext4 /dev/sda1 
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
-Hint -- After you format the first partition (mkfs.ext4 /dev/sda1), you can use the arrow up key and it will repeat the last command. Just change 1 to 2 and press Enter, then 2 to 3 and press Enter. 

lsblk to see mounted drives

## Mount Partitions (MBR-Legacy Machines)
mount /dev/sda2 /mnt
mkdir /mnt/{boot,home}
mount /dev/sda1 /mnt/boot
mount /dev/sda3 /mnt/home
-Hint --You can do the same thing with the arrow up key to repeat things and change as necessary.

lsblk again to see mounted drives

You shoule see something like:
-sda1 0:1   0    300M  0 part /mnt/boot
-sda2 0:2   0     25G  0 part /mnt
-sda3 0:3   0     94G  0 part /mnt/home <--- This will show whatever the remaining drive space should be for your drive. On mine it was 94G (Gigabytes)

## Install main packages
pacstrap /mnt base linux linux-firmware vim git

## Generate File System Table
genfstab -U /mnt >> /mnt/etc/fstab 
arch-chroot /mnt
cat /etc/fstab (Shows the fstab file contents)

## Set Timezone
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc

## Create /etc/locale.gen
vim /etc/locale.gen
-- Use / then en to locate the line needed to edit (en_US)
-- Select line with #en_US.UTF-8 UTF-8
-- Press i for insert/edit mode
-- delete # at the beginning of the line
-- Press esc to escape
-- Press :wq or :x to Write and Quit vim

##Create locale-gen
locale-gen

##Create /etc/locale.conf
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

## Create /etc/hostname
echo "YourComputerName" >> /etc/hostname 
IMPORTANT NOTE: For "YourComputerName" you can put anything you want there. Something easy to remember preferably. Your last name, nickname, dogs name, bank name... whatever you want.

## Create /etc/hosts
vim /etc/hosts (remember in vim, i = insert or edit text, escape = manage file by saving and exiting it with :x)

127.0.0.1	localhost
::1			localhost
127.0.1.1	YourComputerName.localdomain		YourComputerName

:x to exit vim and save

## Root User Password Change - changes the root user password. I usually use the password I use for my username. It's easier to remember.
passwd
(type password)
(retype password)

## Add Main Packages
pacman -Syu grub dhcpcd networkmanager network-manager-applet dialog wpa_supplicant base-devel linux-headers xdg-utils xdg-user-dirs bluez bluez-utils cups pulseaudio pipewire alsa-utils pavucontrol terminus-font 

## Install grub bootloader
grub-install --target=i386-pc --recheck /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

## Enable System Tools
systemctl enable NetworkManager 
systemctl enable bluetooth
systemctl enable cups

## Add User
useradd -m YourUserName
--Check with cat /etc/passwd

## Change your user password
passwd YourNameHere
(type password)
(retype password)

## Enable the wheel group for sudo priveliges
EDITOR=vim visudo
--Scroll to # %wheel ALL=(ALL) ALL
--Remove #

## Add your user to the wheel group
usermod -aG wheel YourNameHere

## Install xorg
sudo pacman -S xorg-server xorg-xrandr

## Finish Up
exit
umount -R /mnt
reboot

##EXTRAS (optional)
--At GRUB Boot Screen type "e" to edit grub menu
--Arrow down to linux line and add "video=1920x1080" to the end of the line (without the quotes)
--CTRL x to reboot

## Do a system Update before continuing on
sudo pacman -Syu

Login with your username and password
setfont ter-132n

##Check Internet connection
ip -c a

ping google.com

## Install Paru AUR Helper
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

## EXTRA THINGS TO INSTALL (Note, Use paru, not pacman, because some packages are in the AUR as well):
paru -Sy geany arandr exa emacs celluloid eog thunderbird htop gimp libreoffice gscreenshot galculator xarchiver dmenu lxappearence nitrogen archlinux-wallpaper pcmanfm ranger firefox sddm terminator alacritty audacium okular

## CREATE SWAP FILE (This one you should probably do)
- fallocate -l 2g /swapfile
- chmod 600 /swapfile
- mkswap /swapfile
- cp /etc/fstab /etc/fstab.bak
- echo '/swapfile none swap 0 0' | tee -a /etc/fstab
- cat /etc/fstab

--Optional for Sway Installation (Wayland) For Seasoned Linux Users ONLY! Not for beginners!
Install Sway Window Manager (Wayland)
paru -S wlroots-git sway-git
(use the first option for Rust)

## The next steps are completely up to you. Use https://archlinux.org to look at desktop environments (DE) or window managers (WM). Cinnamon is a great DE for beginners.

## The following is what I usually install in VMs. 
sudo pacman -S 


Cinnamon is a very nice desktop environment. i3 is a tiling windows manager. Don't install this if you don't know how to use it. Stick with Cinnamon Desktop. It's very much like Windows. dmenu is a nice menu system, lxappearance is a window manager window appearance manager, nitrogen is a nice wallpaper selector, pcmanfm is a nice file manager, ranger is a terinal based file manager, sddm is a nice login manager, terminator and alacritty are nice terminal emulators. You don't need both but both are nice and easy to use.

sudo systemctl enable sddm
reboot

Login with your environment of choice

##i3 Window Manager
Enter to Create Config file
Enter to accept Mod Key

Mod Key Enter Opens Terminator (default terminal)

Restart System
exit
umount -R /mnt
reboot now
