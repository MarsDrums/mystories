# MyStories

These are some stories I've written along with the notes I use when installing Arch Linux.

- Edited Decades of Linux 		August 22, 2020

- Added Arch-Linux-Install-dos	January 28, 2022 (My personal Arch Linux Installation Notes... They work!)

- Deleted Decades of Linux 		January 29, 2022

- Added My Love For Linux 		January 29, 2022 (Replaces Decades of Linux)

About the Installation Notes:

The Arch Linux Install DOS notes describes in step by step details the process I use to install a non-UEFI version of Arch Linux. I've tested this a few times in Virtual Machines (VMs) and on personal computer hardware (the system I'm writing this README.md file on). All attempts were sucessful.

Everything up to line 162 is necessary to boot into and run Arch Linux. Everything after that is just things that I like to put on MY systems. However, you could choose to put what I have in my notes after line 146 if you wish to do so. However, currently, that is all written in me speak (I probably understand it more than most anyone else would) but these are unpublished so they are still a bit messy to read but I know exactly what's going on. 

If you have any questions about this document, you may be able to reach me on Reddit (my username is Phydoux there as well). I will do my best to answer any questions that arise but most of the stuff on here is pretty easy to figure out.

The document makes reference to https://gitlab.com/Phydoux/archlinux. I have that private because I'd like to add a few things to it before I make it publicly available.
